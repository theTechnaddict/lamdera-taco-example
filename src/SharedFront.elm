module SharedFront exposing (init, update)

import Browser.Navigation exposing (Key)
import Types exposing (SharedFront, SharedFrontMsg(..))
import Url exposing (Url)


init : Url -> Key -> SharedFront
init url navKey =
    { url = url
    , navKey = navKey
    , counter = 0
    , clientId = ""
    }


update : SharedFront -> SharedFrontMsg -> SharedFront
update state msg =
    case msg of
        WelcomeReceived ->
            { state
                | url = state.url
                , navKey = state.navKey
                , counter = state.counter
                , clientId = state.clientId
            }

        NewValueReceived v ->
            { state | counter = v }

        NoUpdate ->
            state
