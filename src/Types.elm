module Types exposing (..)

import Browser exposing (UrlRequest(..))
import Browser.Navigation
import Lamdera exposing (ClientId)
import Set exposing (Set)
import Url exposing (Url)


type alias BackendModel =
    { counter : Int
    , clients : Set ClientId
    }


type alias FrontendModel =
    { navKey : Browser.Navigation.Key
    , url : Url
    , appState : AppState
    }


type AppState
    = NotReady
    | Ready SharedFront RouterModel
    | FailedToInitialize


type alias SharedFront =
    { navKey : Browser.Navigation.Key
    , url : Url
    , counter : Int
    , clientId : String
    }


type alias RouterModel =
    { homeModel : HomeModel
    , counterModel : CounterModel
    , route : Route
    }


type FrontendMsg
    = UrlChange Url
    | LinkClicked UrlRequest
    | GotRouterMsg RouterMsg
    | FNoop


type SharedFrontMsg
    = WelcomeReceived
    | NewValueReceived Int
    | NoUpdate



-- Router --


type Route
    = HomeRoute
    | CounterRoute
    | NotFoundRoute


type RouterMsg
    = RouterUrlChange Url
    | NavigateTo Route
    | GotHomeMsg HomeMsg
    | GotCounterMsg CounterMsg



-- Pages --
-- Home page


type alias HomeModel =
    {}


type HomeMsg
    = HomeNoOp



-- Counter page


type alias CounterModel =
    {}


type CounterMsg
    = Incremented
    | Decremented


type ToBackend
    = ClientJoin
    | CounterIncremented
    | CounterDecremented


type BackendMsg
    = Noop


type ToFrontend
    = WelcomeClient Int String
    | CounterNewValue Int
