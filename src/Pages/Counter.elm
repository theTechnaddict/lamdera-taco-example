module Pages.Counter exposing (..)

import Browser.Navigation exposing (pushUrl)
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (css)
import Html.Styled.Events exposing (..)
import Lamdera exposing (sendToBackend)
import Routing.Helpers exposing (reverseRoute)
import Styles exposing (..)
import Types exposing (CounterModel, CounterMsg(..), Route(..), SharedFront, SharedFrontMsg(..))


type alias Model =
    CounterModel


init : ( Model, Cmd CounterMsg )
init =
    ( {}
    , Cmd.none
    )


update : SharedFront -> CounterMsg -> Model -> ( Model, Cmd CounterMsg, SharedFrontMsg )
update shared msg model =
    case msg of
        Incremented ->
            ( model
            , sendToBackend Types.CounterIncremented
            , Types.NoUpdate
            )

        Decremented ->
            ( model
            , sendToBackend Types.CounterDecremented
            , Types.NoUpdate
            )


view : SharedFront -> Model -> Html CounterMsg
view shared model =
    div []
        [ button
            [ onClick Decremented ]
            [ text "-" ]
        , text (String.fromInt shared.counter)
        , button
            [ onClick Incremented ]
            [ text "+" ]
        ]
