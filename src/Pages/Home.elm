module Pages.Home exposing (..)

import Html.Styled as Html exposing (..)
import Html.Styled.Attributes exposing (href, src)
import Html.Styled.Events exposing (..)
import Styles exposing (..)
import Types exposing (HomeModel, HomeMsg, SharedFront)


type alias Model =
    HomeModel


type alias Msg =
    HomeMsg


init : ( Model, Cmd Msg )
init =
    ( {}
    , Cmd.none
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Types.HomeNoOp ->
            ( model, Cmd.none )


view : SharedFront -> Model -> Html Msg
view shared model =
    div [ styles appStyles ]
        [ h1 [] [ text "Home, sweet" ]
        , p []
            [ text "And a special guest from the other page: "
            , text (String.fromInt shared.counter)
            ]
        ]
