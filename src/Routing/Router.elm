module Routing.Router exposing (..)

import Browser
import Browser.Navigation exposing (Key)
import Html
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (href)
import Html.Styled.Events exposing (..)
import Pages.Counter as Counter
import Pages.Home as Home
import Routing.Helpers exposing (parseUrl, reverseRoute)
import Styles exposing (..)
import Types
    exposing
        ( CounterMsg
        , HomeMsg
        , Route(..)
        , RouterModel
        , RouterMsg(..)
        , SharedFront
        , SharedFrontMsg(..)
        )
import Url exposing (Url)


type alias Model =
    RouterModel


init : Url -> ( Model, Cmd RouterMsg )
init url =
    let
        ( homeModel, homeCmd ) =
            Home.init

        ( counterModel, counterMsg ) =
            Counter.init
    in
    ( { homeModel = homeModel
      , counterModel = counterModel
      , route = parseUrl url
      }
    , Cmd.map GotHomeMsg homeCmd
    )


update : SharedFront -> RouterMsg -> Model -> ( Model, Cmd RouterMsg, SharedFrontMsg )
update shared msg model =
    case msg of
        RouterUrlChange location ->
            ( { model | route = parseUrl location }
            , Cmd.none
            , NoUpdate
            )

        NavigateTo route ->
            ( model
            , Browser.Navigation.pushUrl shared.navKey (reverseRoute route)
            , NoUpdate
            )

        GotHomeMsg homeMsg ->
            updateHome model homeMsg

        GotCounterMsg counterMsg ->
            updateCounter shared model counterMsg


updateHome : Model -> HomeMsg -> ( Model, Cmd RouterMsg, SharedFrontMsg )
updateHome model homeMsg =
    let
        ( nextHomeModel, homeCmd ) =
            Home.update homeMsg model.homeModel
    in
    ( { model | homeModel = nextHomeModel }
    , Cmd.map GotHomeMsg homeCmd
    , NoUpdate
    )


updateCounter : SharedFront -> Model -> CounterMsg -> ( Model, Cmd RouterMsg, SharedFrontMsg )
updateCounter shared model counterMsg =
    let
        ( nextCounterModel, counterCmd, sharedFrontMsg ) =
            Counter.update shared counterMsg model.counterModel
    in
    ( { model | counterModel = nextCounterModel }
    , Cmd.map GotCounterMsg counterCmd
    , sharedFrontMsg
    )


view : (RouterMsg -> msg) -> SharedFront -> Model -> Browser.Document msg
view msgMapper shared model =
    let
        buttonStyles route =
            if model.route == route then
                styles navigationButtonActive

            else
                styles navigationButton

        title =
            case model.route of
                HomeRoute ->
                    "Home"

                CounterRoute ->
                    "Counter"

                NotFoundRoute ->
                    "404"

        body =
            div [ styles (appStyles ++ wrapper) ]
                [ header [ styles headerSection ]
                    [ h1 [] [ text "site-title" ]
                    ]
                , nav [ styles navigationBar ]
                    [ button
                        [ onClick (NavigateTo HomeRoute)
                        , buttonStyles HomeRoute
                        ]
                        [ text "home" ]
                    , button
                        [ onClick (NavigateTo CounterRoute)
                        , buttonStyles CounterRoute
                        ]
                        [ text "counter" ]
                    ]
                , pageView shared model
                , footer [ styles footerSection ]
                    [ text "Made with elm, lamdera and elm-shared-state" ]
                ]
    in
    { title = title ++ " - Elm Shared State Demo"
    , body =
        [ body
            |> Html.Styled.toUnstyled
            |> Html.map msgMapper
        ]
    }


pageView : SharedFront -> Model -> Html RouterMsg
pageView shared model =
    div [ styles activeView ]
        [ case model.route of
            HomeRoute ->
                Home.view shared model.homeModel
                    |> Html.Styled.map GotHomeMsg

            CounterRoute ->
                Counter.view shared model.counterModel
                    |> Html.Styled.map GotCounterMsg

            NotFoundRoute ->
                h1 [] [ text "404 :(" ]
        ]
