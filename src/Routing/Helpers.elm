module Routing.Helpers exposing (parseUrl, reverseRoute, routeParser)

import Types exposing (Route(..))
import Url exposing (Url)
import Url.Parser exposing ((</>))


reverseRoute : Route -> String
reverseRoute route =
    case route of
        CounterRoute ->
            "#/counter"

        _ ->
            "#/"


routeParser =
    Url.Parser.oneOf
        [ Url.Parser.map HomeRoute Url.Parser.top
        , Url.Parser.map CounterRoute (Url.Parser.s "counter")
        ]


parseUrl : Url -> Route
parseUrl url =
    case url.fragment of
        Nothing ->
            HomeRoute

        Just fragment ->
            { url | path = fragment, fragment = Nothing }
                |> Url.Parser.parse routeParser
                |> Maybe.withDefault NotFoundRoute
