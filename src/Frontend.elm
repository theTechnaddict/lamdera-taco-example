module Frontend exposing (Model, app)

import Browser exposing (UrlRequest(..))
import Browser.Navigation
import Html exposing (Html, text)
import Html.Attributes exposing (style)
import Html.Events exposing (onClick)
import Lamdera exposing (sendToBackend)
import Routing.Router as Router
import SharedFront
import Types
    exposing
        ( AppState(..)
        , FrontendModel
        , FrontendMsg(..)
        , RouterModel
        , RouterMsg(..)
        , SharedFront
        , SharedFrontMsg(..)
        , ToBackend(..)
        , ToFrontend(..)
        )
import Url exposing (Url)


type alias Model =
    FrontendModel


{-| Lamdera applications define 'app' instead of 'main'.

Lamdera.frontend is the same as Browser.application with the
additional update function; updateFromBackend.

-}
app =
    Lamdera.frontend
        { init = init
        , update = update
        , updateFromBackend = updateFromBackend
        , view = view
        , subscriptions = \_ -> Sub.none
        , onUrlChange = UrlChange
        , onUrlRequest = LinkClicked
        }


init : Url -> Browser.Navigation.Key -> ( Model, Cmd FrontendMsg )
init url navKey =
    ( { url = url
      , navKey = navKey
      , appState = NotReady
      }
    , sendToBackend ClientJoin
    )


update : FrontendMsg -> Model -> ( Model, Cmd FrontendMsg )
update msg model =
    case msg of
        UrlChange url ->
            updateRouter { model | url = url } (RouterUrlChange url)

        GotRouterMsg routerMsg ->
            updateRouter model routerMsg

        LinkClicked urlRequest ->
            case urlRequest of
                Internal url ->
                    ( model, Browser.Navigation.pushUrl model.navKey (Url.toString url) )

                External url ->
                    ( model, Browser.Navigation.load url )

        FNoop ->
            ( model, Cmd.none )


updateRouter : Model -> RouterMsg -> ( Model, Cmd FrontendMsg )
updateRouter model routerMsg =
    case model.appState of
        Ready sharedFront routerModel ->
            let
                nextSharedFront =
                    SharedFront.update sharedFront sharedFrontMsg

                ( nextRouterModel, routerCmd, sharedFrontMsg ) =
                    Router.update sharedFront routerMsg routerModel
            in
            ( { model | appState = Ready nextSharedFront nextRouterModel }
            , Cmd.map GotRouterMsg routerCmd
            )

        _ ->
            ( model, Cmd.none )


updateFromBackend : ToFrontend -> Model -> ( Model, Cmd FrontendMsg )
updateFromBackend msg model =
    case ( model.appState, msg ) of
        ( NotReady, WelcomeClient newValue clientId ) ->
            let
                newShared =
                    SharedFront.update
                        { url = model.url
                        , navKey = model.navKey
                        , counter = newValue
                        , clientId = clientId
                        }
                        WelcomeReceived

                newRouterModel =
                    Tuple.first <| Router.init model.url
            in
            ( { model | appState = Ready newShared newRouterModel }
            , Cmd.none
            )

        ( Ready sharedState routerModel, CounterNewValue newValue ) ->
            let
                newShared =
                    { sharedState | counter = newValue }
            in
            ( { model | appState = Ready newShared routerModel }
            , Cmd.none
            )

        _ ->
            ( model, Cmd.none )


view : Model -> Browser.Document FrontendMsg
view model =
    case model.appState of
        Ready sharedFront routerModel ->
            Router.view GotRouterMsg sharedFront routerModel

        NotReady ->
            { title = "loading app"
            , body = [ text "loading..." ]
            }

        FailedToInitialize ->
            { title = "not really an app"
            , body = [ text "I'm off to bed..." ]
            }
